#Carlos Padron
"""
Creacion de variable placa para el inicio de el while loop.
"""
Placa = 1
"""
Creacion de los Diccionarios para la validacion de las placas. Sus nombres van dependiendo de la cantidad de 
Numeros que se encuentras al final de la placa. 
"""
placas2 = { "MADM" : " Placa diplomatica"}

placas3 = { "RCD" : " Placa diplomatica",
 "MCD" : " Placa diplomatica",
 "ADM" : " Placa de administracion",
 "MMI" : " Placa diplomatica",
 "RMI" : " Placa diplomatica",
 "BC" : " Bus escolar"}

placas4 = { "CP" : " Autoridad del Canal",
 "CD" : " Cuerpo Diplomatico",
 "PH" : " Placa diplomatica",
 "PE" : " Placa diplomatica",
 "PR" : " Periodista",
 "HP" : " Radioaficionados",
 "MB" : " Metrobus",
 "MA" : " Motos",
 "T" : " Taxis",
 "B" : " Buses",
 "CC" : " Placa diplomatica",
 "CH" : " Placa diplomatica",
 "MI" : " Placa diplomatica"}

placas5 = { " D " : " Demostracion",
 "E" : " Fiscales y Jueces",
 "T" : " Taxis",
 "G" : " Gobierno"}
"""
Diccionario usado para saber que otro diccionario sera utilizado.
Esto depende de la cantidad de numeros al final de la placa.
"""
dictplacas = {2:placas2,3:placas3,4:placas4,5:placas5}
"""Lista de Provincias para las placas por provincias."""
provincias = {"1" : "Bocas Del Toro","2" : "Coclé","3" : "Colón","4" : "Chiriquí","5" : "Darién","6" : "Herrera",
"7" : "Los Santos","8" : "Panamá","9" : "Veraguas"}
"""
El programa inicial. 
"""
def entrada():
    """
    :return: True
    """
    print("0 para salir.")
    """Entrada de la Placa"""
    Placa = input("Introdusca el Numero de la Placa:  ")
    """Estandarizaion de la placa para que todas las letras sean mayusculas. eliminaria errores """
    Placa = Placa.upper()
    return (True,Placa)

def inicio(Placa):
    """
    :return:
    """
    """Salida de el Programa"""
    if Placa == "0":
        exit()
        """Primera Validacion de la placa, que contenga esactamente 6 caracteres usando len(). De lo contrario el programa no 
        continua. Se pide que toque enter para continuar y se repite los pasos anteriores."""
    if len(Placa) != 6:
        print("""Placa incorrecta, debe tener 6 caracteres.""")
        """Revision si los 6 digitos de la placa son numeros. Usando isnumeric(). Si los 6 digitos son numeros entonces 
        esta validada la placa. Da el mensaje de placa correcta seguido por reinicio de el loop para seguir con la 
        siguiente placa a ser validada"""
        return [False]
    if Placa.isnumeric() == True:
        print("La Placa con numeracion ",Placa," es Valida. Placa de Auto Particular descontinuada.")
        input("Precione enter para Continuar...")
        return [False]
    else:
        """En caso de no ser todos numeros primero se determina cuantos valores al final de la placa son numeros.
        En este caso se crearon unas variables para ser guardar la unformacion."""
        n = -1
        ntemp = 1
        cantnumero = 0
        prueba = True
        """Un loop que pueba uno por uno los caracteres de la placa, de el ultimo hasta la primera letra / simbolo. 
        se usa la variable ntemp para guardar el valor de un digito de la placa. luego se prueba si ese digito es 
        un numero.
        """
        while prueba == True:
            ntemp = Placa[n]
            prueba = ntemp.isnumeric()
            """Si el valor de ntemp conseguido arriba es un numero entonces se le suma 1 a la cantidad de numeros al 
            final de la placa con la variable con nombre cantnumero. adicional se le -1 a la variable n para que 
            agarre el siguiente valor de el ultimo hacia el primero"""
            if prueba == True:
                cantnumero = cantnumero + 1
                n = n - 1
            else:
                """Una ves terminada la recolecta de informacion de arriba, averiguo si el primer valor de la placa es
                numerico, ya que en algunos casis se usa ese caracter para marcar la provincia de dicho vaiculo"""
                primero = Placa[0]
                primer = primero.isnumeric()
                """Paso los valores hacia el primer filtro de las placas. Se pasan las informacion de las variables 
                que se necesitan. """
                #filtro_1(Placa,cantnumero,primer,primero)
                return (True,Placa,cantnumero,primer,primero)
"""Los Filtros por diccionarios. Recibe los valores conseguidos en la fase anterior. """
def filtro_1():
    """
    :param Placa: En numero de la Placa ya estandarizada a letras en mayuscula.
    :param cantnumero: Contador de cuantos numeros ha al final de la placa
    :param primer: Bool si el primer caracter de la placa es un numero.
    :param primero: El primer caracter de la placa
    :return:
    """
    Placa = k[1]
    cantnumero = k[2]
    primer = k[3]
    primero = k[4]
    """Se selecciona que lista va a ser usada, dependiendo de la cantidad de numeros al final de la placa. En este
    caso, solo queda un posible error que se puede capturar antes de revisar las listas. seria que tuviera un total
    de 0 o 1 caracteres numericos al final de la placa. ya que ninguna de las posibles placas tienen esa opcion.
    Si uno de esos dos valores son encontrados, listausada tendria un valor de Falso, el cual se usaria en el paso
    sigueinte para invalidar la placa. De ser cualquier otro posible cantidad, se seleccionaria la lista adecuada"""
    listausada = dictplacas.get(cantnumero,False)
    """En caso de que sea 1 o 0 caracteres numericos al final de la placa. Error y se Reinicia el programa."""
    if listausada == False:
        input("Placa no Valida. Precione enter para continuar.")
        return False
    """Para saber la cantidad de Letras posibles al prinsipio de la placa, de saca restando la cantidad de numeros al
    final de la placa a 6 que es el total de caracteres de las mismas. """
    cantletras = 6-cantnumero
    """Para poder posicionar los caracteres correctos en la placa, primero verifica si el primer valor de la misma es
    numerico, ya que en algunos casos es usado para marcar la provincia de la placa. Se le da un valor a la variable
    posiletras, para saber desde que punto de la placa se deben agarrar los valores para la validacion."""
    if primer == True:
        posiletras = 1
        """En el caso de tener un numero frente a la placa. se determina de que provincia es."""
        if primero == "0":
            estado1 = ""
        else:
            estado1 = " La placa es de "+provincias[primero]
    else:
        posiletras = 0
        estado1 = ""
    """Se crea una variable temporal con los valores alfabeticos de la placa. Con la cual se buscara en la lista
    para ser validado. """
    tempkmk = Placa[posiletras:cantletras]
    """La variable Validacion busca en el disccionario establecido las letras conseguidas en la parte anterior.
    si son encontradas entonces la placa es valida. De no ser encontradas pasa al siguiente filtro."""
    Validacion = listausada.get(tempkmk,False)
    """Las placas mas comunes son las de auto particular. Son limitadas a comenzar con dos letras entre AA y AZ o CA
    hasta CZ con las exepciones de las que se encuentran en las tablas de arriba."""
    if Validacion == False:
        if (Placa[0] == "A" or Placa[0] == "C") and Placa[1].isalpha() == True and cantnumero == 4:
            print("La Placa con numeracion ",Placa," es Valida., Auto Particular. ")
            return True
        else:
            input("Placa no Valida. Precione enter para continuar.")
            return False
    else:
        print("La Placa con numeracion "+Placa+" es Valida."+Validacion+"."+estado1)
        return True



while Placa != "0":
    Placatemp = entrada()
    Placa = Placatemp[1]
    k = inicio(Placa)
    if k[0] == True:
        g = filtro_1()
